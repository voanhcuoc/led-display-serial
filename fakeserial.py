from io import BytesIO
from serial import Serial

class FakeSerial(Serial):
    def __init__(self, peer, debug=True):
        super().__init__()
        self.debug = debug
        self.peer = peer
        self.buffer = BytesIO()

    def read(self, size=1):
        data = self.buffer.read(size)
        if self.debug and data:
            print('read: fake serial read: {}'.format(repr(data)))
            print('read: string decoded: {}'.format(repr(bytes(data).decode('utf-8'))))
        return data

    def write(self, data):
        if self.debug:
            print('write: fake serial write: {}'.format(repr(data)))
            print('write: string decoded: {}'.format(repr(bytes(data).decode('utf-8'))))
        n = self.peer.buffer.write(data)
        self.peer.buffer.seek(-n, 1)
        return n
    
    def flush(self):
        pass

def createPairFakePorts(debug=True):
    left = FakeSerial(None, debug)
    right = FakeSerial(left, debug)
    left.peer = right

    return (left, right)
