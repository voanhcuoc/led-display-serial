import sys
from PyQt5 import QtCore, QtGui, QtWidgets

from serial import Serial
from serial.tools import list_ports

from fakeserial import createPairFakePorts

DEBUG = False
RESEND_INTERVAL = 300

STX = chr(0x02)
ETX = chr(0x03)
# STX = 's'
# ETX = 'e'

(left, right) = createPairFakePorts()

allports = list_ports.comports();
print(allports);
if len(allports):
    port = Serial(allports[0].device, timeout=0); # pick first port
else:
    port = None

class UIBase(object):
    def normalized(self, widget, x, y, width, height):
        cpc = self.main_size
        cpc_h = cpc.height()
        cpc_w = cpc.width()
        widget.setGeometry(x*cpc_w, y*cpc_h, width*cpc_w, height*cpc_h)

    def setup(self, main: QtWidgets.QWidget, app):
        main.resize(400, 200)
        self.main_size = main.size()
        
        self.app = app
        self.frame = None

        self.selectPortLabel = QtWidgets.QLabel(main)
        self.selectPortLabel.setText('Select serial port')

        self.selectPortDropdown = QtWidgets.QComboBox(main)
        allportnames = map(lambda port: port.device, allports)
        self.selectPortDropdown.addItems(allportnames)
        currentText = self.selectPortDropdown.currentText()
        if (currentText):
            self.changePort(self.selectPortDropdown.currentText())
        else:
            self.serial = left
        self.selectPortDropdown.currentTextChanged.connect(self.changePort)

        self.stateInputLabel = QtWidgets.QLabel(main)
        self.stateInputLabel.setText('Input')

        self.stateInputField = QtWidgets.QTextEdit(main)

        self.sendButton = QtWidgets.QPushButton(main)
        self.sendButton.setText('Send')
        self.sendButton.clicked.connect(self.send)

        self.currentDisplayLabel = QtWidgets.QLabel(main)
        self.currentDisplayLabel.setText('Current display:')

        self.currentStateLabel = QtWidgets.QLabel(main)
        
        main.show()
        
        # self.normalized(self.overworldLogoLabel, 0.7, 0.02, 0.1, 0.1)
        self.normalized(self.selectPortLabel, 0.1, 0.1, 0.3, 0.2)        
        self.normalized(self.selectPortDropdown, 0.5, 0.1, 0.3, 0.2)
        self.normalized(self.stateInputLabel, 0.1, 0.35, 0.3, 0.2)
        self.normalized(self.stateInputField,      0.5, 0.35, 0.3, 0.2)
        self.normalized(self.sendButton,      0.85, 0.35, 0.1, 0.2)
        self.normalized(self.currentStateLabel,      0.5, 0.65, 0.3, 0.2)
        self.normalized(self.currentDisplayLabel,      0.1, 0.65, 0.3, 0.2)
        
    def changePort(self, text):
        self.serial = Serial(text, timeout=0, baudrate=1200)
        
    def send(self):
        content = self.stateInputField.toPlainText()
        frame = (STX + content + ETX + self.checksum(content)).encode('ascii')
        self.serial.write(frame)
        self.currentStateLabel.setText(content)
        self.frame = frame
    
    def resend(self):
        if self.frame:
            self.serial.write(self.frame)
        
        # debug
        if DEBUG and self.serial:
            # print("read")
            print(self.serial.readline())
            # self.serial

    def checksum(self, content):
        return str(sum(map(ord, iter(content))) % 10)

if __name__ == '__main__':
    # setup UI
    UI = UIBase()
    app = QtWidgets.QApplication(sys.argv)
    main = QtWidgets.QWidget()
    
    UI.setup(main, app)

    timer = QtCore.QTimer()
    timer.setInterval(RESEND_INTERVAL)
    timer.timeout.connect(UI.resend)
    timer.start()

    sys.exit(app.exec_())
    print('now we go')