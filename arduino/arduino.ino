#include <SoftwareSerial.h>
#include <LedControl.h>

#define BUFSIZE 10

//#define STX 's'
//#define ETX 'e'

#define STX 0x02
#define ETX 0x03

// serial pins connect to UART-RS232 converter
#define RX 13
#define TX 14

//SoftwareSerial rs232(RX, TX);
HardwareSerial& rs232 = Serial;

#define MAXINDEXLED 1

LedControl lc=LedControl(12,10,11,2);  // Pins: DIN,CLK,CS,

void blinkled() {
  digitalWrite(LED_BUILTIN, HIGH);
  delay(10);
  digitalWrite(LED_BUILTIN, LOW);
}

void setup() {
  // rs232 setup
  pinMode(RX, INPUT);
  pinMode(TX, OUTPUT);
  rs232.begin(1200);

  lc.shutdown(0,false);  // Wake up displays
  lc.shutdown(1,false);
  lc.setIntensity(0,5);  // Set intensity levels
  lc.setIntensity(1,5);
  lc.clearDisplay(0);  // Clear Displays
  lc.clearDisplay(1);

  // debug & mock setup
//  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
};

char buffer[BUFSIZE];
int bl = 0; // buffer length

void loop() {
  // put your main code here, to run repeatedly:
  bool got_valid_frame = frameread();
  if (got_valid_frame) display();
};

bool frameread() {
  bool parsing = false;
  int i = 0;
  while (true) {
//    Serial.println("try get char");
    if (rs232.available() == 0) continue;
    char next = rs232.read();
//    Serial.println("new char");
//    Serial.println(next);
    if (!parsing) {
      if (next == STX) {
//        Serial.println("catch STX");
        parsing = true;
      }
    } else {
      if (next == ETX) {
        Serial.println("catch ETX");
        while (rs232.available() == 0);
//        Serial.println(". checksum =");
        char checksumchar = rs232.read();
//        Serial.println(checksumchar);
        int checksum = checksumchar - 0x30;
//        Serial.println(checksum);
        int hash = 0;
        for (int j=0; j<i; j++) {
          hash += buffer[j];
          hash %= 10;
        };
//        Serial.println("calculated hash=");
//        Serial.println(hash);
        
        if (hash == checksum) {
//          Serial.println("checksum ok");
          bl = i;
          return true;
        } else {
//          Serial.println("checksum fail, error detected");
          bl = 0;
          return false; // frame error detected, discard
        };
      }
      if (i == BUFSIZE) {
//        Serial.println("overflow");
        bl = 0;
        return false; // buffer overflow, discard
      }
      
//      Serial.println("save to buf");
      buffer[i] = next;
      i++;
    }
  }
};

void display() {
//  Serial.println("display");
  for (int i=0; i<bl; i++) {
    displaychar(i, buffer[i]);
  };
};

////mock
//char digit0 = '0';
//char digit2 = '2';

int digit0[] =
{
   B111,
   B101,
   B101,
   B101,
   B111
};
int digit1[] =
{
   B010,
   B010,
   B010,
   B010,
   B010
};
int digit2[] =
{
   B111,
   B001,
   B111,
   B100,
   B111
};
int digit3[] =
{
   B111,
   B001,
   B111,
   B001,
   B111   
};
int digit4[] =
{
   B101,
   B101,
   B111,
   B001,
   B001
};
int digit5[] =
{
   B111,
   B100,
   B111,
   B001,
   B111
};
int digit6[] =
{
   B111,
   B100,
   B111,
   B101,
   B111
};
int digit7[] =
{
   B111,
   B001,
   B001,
   B001,
   B001
};
int digit8[] =
{
   B111,
   B101,
   B111,
   B101,
   B111
};
int digit9[] =
{
   B111,
   B101,
   B111,
   B001,
   B111
};
int unidentified[] = {
  B000,
  B101,
  B010,
  B101,
  B000
};

void displaychar(int i, char c) {
  switch (c) {
    case '0':
    setled(i, digit0);
    break;
    case '1':
    setled(i, digit1);
    break;
    case '2':
    setled(i, digit2);
    break;
    case '3':
    setled(i, digit3);
    break;
    case '4':
    setled(i, digit4);
    break;
    case '5':
    setled(i, digit5);
    break;
    case '6':
    setled(i, digit6);
    break;
    case '7':
    setled(i, digit7);
    break;
    case '8':
    setled(i, digit8);
    break;
    case '9':
    setled(i, digit9);
    break;
    default:
    setled(i, unidentified);
  }
};

int mask1[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
int mask2[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
int *mask[2] = { mask1, mask2 };

void setled(int index, int* digit) {
    if (index >= 4) return;
    // redraw buffer
    for (int i=0; i<5; i++) {
      int offset = 1 + (1 - (index%2)) * 4;
      mask[index/2][i+1] &= ~ (B111 << offset);   // clear
      mask[index/2][i+1] |= (digit[i] << offset); // render
    };

    // flush buffer
    for (int i = 0; i < 8; i++)  
    {
      lc.setRow(MAXINDEXLED - (index/2),i,mask[index/2][i]);
    };
};
